-- ----------------------------
-- Table structure for university
-- ----------------------------
DROP TABLE IF EXISTS `university`;
CREATE TABLE `university` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(20) DEFAULT NULL,
  `university_desc` varchar(500) DEFAULT NULL,
  `university_name` varchar(100) DEFAULT NULL,
  `rank` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大学表';


INSERT INTO `university` VALUES ('1', '北京', '985', '清华大学','1');
INSERT INTO `university` VALUES ('2', '北京', '985', '北京大学', '2');
INSERT INTO `university` VALUES ('3', '上海', '985', '复旦大学', '3.x');
INSERT INTO `university` VALUES ('4', '浙江', '983', '浙江大学', '3.x');
