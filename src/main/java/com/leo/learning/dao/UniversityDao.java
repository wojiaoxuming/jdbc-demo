package com.leo.learning.dao;

import com.leo.learning.dto.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author lcli
 * @date 2020/9/9
 */
@Repository
public class UniversityDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 保存大学
     * @param university
     */
    public Integer save(University university) {

        String sql = "insert into university(university_name,university_desc,city_name,rank) values(?,?,?,?)";
        return jdbcTemplate.update(sql, university.getUniversity_name(), university.getUniversity_desc(), university.getCity_name(), university.getRank());
    }

    /**
     * 删除大学
     * @param
     */
    public Integer delete(Integer id) {
        String sql = "delete from university where id=?";
        return jdbcTemplate.update(sql, id);
    }

    /**
     * 查询全部大学
     * @return
     */
    public List<University> findAll() {
        String sql = "select * from university";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(University.class));
    }
}
