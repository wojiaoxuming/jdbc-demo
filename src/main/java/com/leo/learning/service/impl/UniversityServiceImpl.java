package com.leo.learning.service.impl;

import com.google.common.collect.Maps;
import com.leo.learning.dao.UniversityDao;
import com.leo.learning.dto.University;
import com.leo.learning.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author lcli
 * @date 2020/9/9
 */
@Service
public class UniversityServiceImpl implements UniversityService {
    @Autowired
    private UniversityDao universityDao;

    @Override
    public Integer save(University university) {
        return universityDao.save(university);
    }

    @Override
    public Integer delete(Integer id) {
        return universityDao.delete(id);
    }

    @Override
    public List<University> findAll() {
        return universityDao.findAll();
    }
}
