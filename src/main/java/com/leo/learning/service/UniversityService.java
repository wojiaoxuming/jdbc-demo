package com.leo.learning.service;

import com.leo.learning.dto.University;

import java.util.List;

/**
 * @author lcli
 * @date 2020/9/9
 */
public interface UniversityService {
    /**
     * 保存大学
     * @param university
     */
    public Integer save(University university);

    /**
     * 删除大学
     * @param id
     */
    public Integer delete(Integer id);

    /**
     * 查询全部大学
     * @return
     */
    public List<University> findAll();
}
