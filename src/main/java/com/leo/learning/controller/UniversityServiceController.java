package com.leo.learning.controller;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.leo.learning.dto.University;
import com.leo.learning.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RestController
@RequestMapping("university")
public class UniversityServiceController {

    @Autowired
    private UniversityService universityService;

    @PostMapping(value = "/save")
    public Object save(@RequestBody University university) {
        return universityService.save(university);

    }

    @GetMapping(value = "/delete")
    public Object delete(@RequestParam("id") Integer id) {
        return  universityService.delete(id);
    }

    @GetMapping(value = "/findAll")
    public Object findAll() {
        return universityService.findAll();
    }

    @PostMapping(value = "/setSession")
    public void setSession(@RequestParam("name") String name) {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        HttpSession session = request.getSession();
        session.setAttribute("name", "jdbc-demo");
        session.setMaxInactiveInterval(1800);
        ServletContext ContextA = request.getSession().getServletContext();
        ContextA.setAttribute("session", request.getSession());

    }

    @GetMapping(value = "/getSession")
    @JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
    public Object getSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ServletContext ContextA = session.getServletContext();
        ServletContext ContextB = ContextA.getContext("/service");
        return  ContextB.getAttribute("name");

    }

    @GetMapping("/getRedisSession")
    public String session(HttpServletRequest request) {
        return (String) request.getSession().getAttribute("userName");
    }
}
