package com.leo.learning.dto;

/**
 * @author lcli
 * @date 2020/9/9
 */
public class University {
    private Integer id;

    private String university_name;

    private String university_desc;

    private String rank;

    private String city_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniversity_name() {
        return university_name;
    }

    public void setUniversity_name(String university_name) {
        this.university_name = university_name;
    }

    public String getUniversity_desc() {
        return university_desc;
    }

    public void setUniversity_desc(String university_desc) {
        this.university_desc = university_desc;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
}
